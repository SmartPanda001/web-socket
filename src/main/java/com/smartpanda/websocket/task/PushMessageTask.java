package com.smartpanda.websocket.task;

import com.smartpanda.websocket.server.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @Author: wujiangwei
 * @Date: 2020/6/29 14:27
 * @Description:
 */
@Service
@Slf4j
public class PushMessageTask {

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private WebSocketServer webSocketServer;

    int count = 0;
    @Scheduled(cron = "0/1 * * *  * ?")
    void pushMessage(){
        long num = count++;
        log.info("我要进行第 i = {} 次广播了",num);
        template.convertAndSend("/topic/sendTopic","第"+num+"次广播");

    }
}
