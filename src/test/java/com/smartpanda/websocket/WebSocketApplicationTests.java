package com.smartpanda.websocket;

import com.smartpanda.websocket.config.SocketManager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.socket.WebSocketSession;

@SpringBootTest
class WebSocketApplicationTests {
    @Autowired
    private SimpMessagingTemplate template;

    @Test
    void contextLoads() {
    }

}
